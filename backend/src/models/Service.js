const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const states = {
    values: ['WAITING_ROLE', 'NEXT_ROLE', 'SOLVED_ROLE'],
    message: '{VALUE} no es un rol valido'
};

// Service token is the same service._id

const serviceSchema = new Schema({
    idEmployee: {
        type: Schema.Types.ObjectId,
        required: true,
        trim: true,
    },
    idClient: {
        type: Schema.Types.ObjectId,
        required: true,
        trim: true
    },
    state: {
        type: String,
        required: true,
        default: 'WAITING_ROLE',
        enum: states,
    },
    indications: {
        type: String,
        required: true,
        trim: true,
    },
    description: {
        type: String,
        required: true,
        trim: true,
    },
    comment: {
        type: String,
        trim: true
    },
    calification: {
        type: Number,
        trim: true
    },
    serviceTrackingLink: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    serviceCalificationLink: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
}, {
    timestamps: true
});
serviceSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = model('Service', serviceSchema);