const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const rolesValidos = {
    values: ['CLIENT_ROLE', 'EMPLOYEE_ROLE'],
    message: '{VALUE} no es un rol valido'
};

const userSchema = new Schema({
    rol: {
        type: String,
        required: true,
        default: 'CLIENT_ROLE',
        enum: rolesValidos,
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    cellNumber: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    token: {
        type: String,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    }
}, {
    timestamps: true
});

userSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });
module.exports = model('User', userSchema);