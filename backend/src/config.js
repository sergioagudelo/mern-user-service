if (process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}

// Enviroment
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Database
let urlDB;
if (process.env.NODE_ENV === 'development') {
    urlDB = 'mongodb://localhost/mern-user-services';
} else {
    urlDB = process.env.MONGODB_URI;
}
process.env.URL_DB = 'mongodb+srv://MERNSERVICEUSER:MERNSERVICEUSER@cluster0-qonxu.mongodb.net/mern-user-services';

// Port
process.env.PORT = process.env.PORT || 4000;

// JWT
process.env.JWT_SECRET_KEY = 'secretkey123456';
process.env.JWT_EXPIRES_IN = 24 * 60 * 60;


// "postinstall": "npm run build",
// "build": "cd frontend && npm run build"