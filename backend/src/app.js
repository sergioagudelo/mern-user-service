require('./config.js');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const app = express();

// settings
app.set('port', process.env.PORT);

// middlewares 
app.use(cors({ origin: [process.env.FRONT_URI, 'http://localhost:3000'] }));
app.use(express.json());
app.use(morgan('dev'));

// routes
app.use('/api/services', require('./routes/service'));
app.use('/api/users', require('./routes/users'));

module.exports = app;