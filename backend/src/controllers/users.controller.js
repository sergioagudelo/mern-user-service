const userCtrl = {};

const jwt = require('jsonwebtoken');

const bcrypt = require('bcryptjs');
// Aleatory fragment used to generate the hash next to the password
const salt = 12;

const User = require('../models/User');

// Status codes
// 200, 201, 400, 403, 404, 500

userCtrl.getUsers = async(req, res) => {
    try {
        const _users = await User.find();
        if (!_users) {
            res.status(400).json({
                ok: false,
                message: 'No existen usuarios registrados en la aplicación.',
            });
        }
        //TODO: res acts as a return? breaks the method?
        res.status(200).json({
            ok: true,
            users: _users,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

userCtrl.getEmployees = async(req, res) => {
    try {
        const _users = await User.find({ rol: 'EMPLOYEE_ROLE' });
        if (!_users) {
            res.status(400).json({
                ok: false,
                message: 'No existen usuarios registrados en la aplicación.',
            });
        }
        //TODO: res acts as a return? breaks the method?
        res.status(200).json({
            ok: true,
            users: _users,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

userCtrl.createUser = async(req, res) => {
    const { name, email, cellNumber, password } = req.body;
    const rol = req.body.rol ? req.body.rol : 'CLIENT_ROLE';

    let date = new Date();
    date = date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds();
    const token = jwt.sign({ tokenData: name + date},
        process.env.JWT_SECRET_KEY, {
            expiresIn: process.env.JWT_EXPIRES_IN
        }); 

    const newUser = new User({
        name,
        email,
        cellNumber,
        password: bcrypt.hashSync(password, salt),
        token,
        rol,
    });
    try {
        const _user = await newUser.save();
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo crear el usuario.',
            });
        }
        res.status(201).json({
            ok: true,
            message: 'Usuario creado.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

userCtrl.getUser = async (req, res) => {
    const { id } = req.params;

    try {
        const _user = await User.findById(id);
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el usuario en la aplicación',
            });
        }
        res.status(200).json({
            ok: true,
            user: _user,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

userCtrl.loginUser = async (req, res) => {
    const { email, password } = req.body;

    try {
        const _user = await User.findOne({ email: email });
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el usuario en la aplicación',
            });
        } else {
            const resultPassword = bcrypt.compareSync(password, _user.password);
            if( resultPassword ) {
                res.status(200).json({
                    ok: true,
                    user: _user,
                });
            } else {
                res.status(403).json({
                    ok: false,
                    user: "Contraseña incorrecta.",
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

userCtrl.deleteUser = async(req, res) => {
    const { id } = req.params;

    try {
        const _user = await User.findByIdAndDelete(id);
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo eliminar el usuario.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'User deleted',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

module.exports = userCtrl;