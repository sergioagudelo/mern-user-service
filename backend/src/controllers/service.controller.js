const ServiceCtrl = {};

const Service = require('../models/Service');
const User = require('../models/User');

ServiceCtrl.getServices = async(req, res) => {
    try {
        const _Service = await Service.find();
        if (!_Service) {
            res.status(400).json({
                ok: false,
                message: 'No se han encontrado servicios en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            services: _Service,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

ServiceCtrl.createService = async(req, res) => {
    const { idClient, indications, description } = req.body;
    const dateNow = Date.now();

    const employee = await User.aggregate([
        { $match: { rol: 'EMPLOYEE_ROLE' } },
        { $sample: { size: 1 } },
        { $project: { _id: 1 }}
    ]);

    const serviceTrackingLink = `track${employee[0]._id}${idClient}${dateNow}`;
    const serviceCalificationLink = `calificate${employee[0]._id}${idClient}${dateNow}`;

    const newService = new Service({
        idEmployee: employee[0]._id,
        idClient,
        indications,
        description,
        serviceTrackingLink,
        serviceCalificationLink,
    });

    try {
        const _Service = await newService.save();
        if (!_Service) {
            res.status(400).json({
                ok: true,
                message: 'No se pudo crear el servicio.',
            });
        }
        res.status(201).json({
            ok: true,
            message: 'Servicio creado.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

// 200
// 201
// 400
// 403
// 404
// 500

ServiceCtrl.getService = async(req, res) => {
    const { id } = req.params;

    try {
        const _Service = await Service.findById(id);
        if (!_Service) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el servicio en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            service: _Service,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

ServiceCtrl.getServicesByEmployee = async(req, res) => {
    const { idEmployee } = req.params;

    try {
        const _Service = await Service.find({ idEmployee });
        if (!_Service) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el servicio en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            service: _Service,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

ServiceCtrl.getServicesByClient = async(req, res) => {
    const { idClient } = req.params;

    try {
        const _Service = await Service.find({ idClient: idClient });
        if (!_Service) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el servicio en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            service: _Service,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

ServiceCtrl.deleteService = async(req, res) => {
    const { id } = req.params;
console.log(id)
    try {
        const _Service = await Service.findByIdAndDelete(id);
        if (!_Service) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el servicio en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Servicio eliminado correctamente.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

ServiceCtrl.updateService = async(req, res) => {
    const { idService, state, inidcations, description, comment, calification } = req.body;
    console.log(req.body)
    console.log(idService, comment, calification)
    try {
        const _Service = await Service.findByIdAndUpdate(idService, {
            comment,
            calification,
        }, { new: true });

        console.log(_Service)
        if (!_Service) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo actualizar el comentario en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Comentario actualizado',
            _Service,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

module.exports = ServiceCtrl;