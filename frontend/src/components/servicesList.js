import React, { Component } from 'react'
import axios from 'axios'
import swal from 'sweetalert';
import { format } from 'timeago.js'
import { Link } from 'react-router-dom'

export default class NotesList extends Component {

    state = {
        services: [],
        useCase: false
    }

    async componentDidMount() {
        console.log("a")
        this.getServices();
    }

    getServices = async () => {
        let res;
        console.log(this.props.location.state._idUserLogged)
        this.state.useCase = localStorage.getItem('rol') === 'CLIENT_ROLE' ? true : false;
        if ( this.state.useCase ) {
            res = await axios.get(`http://localhost:4000/api/services/getServicesByClient/${this.props.location.state._idUserLogged}`);
        } else {
            res = await axios.get(`http://localhost:4000/api/services/getServicesByEmployee/${this.props.location.state._idUserLogged}`);
        }
        console.log(res)
        if ( res.data.ok ) {
            console.log(res.data.service)
            this.setState({
                services: res.data.service
            });
        } else {
            swal("Oops!", "Something went wrong!", "error");
        }
    }

    deleteService = async (noteId) => {
        await axios.delete(`http://localhost:4000/api/services/deleteService/${noteId}`);
        this.getServices();
    }

    render() {
        return (
            <React.Fragment>
                <h1 style={{color: "white"}}>You are logged as a {localStorage.getItem('rol')}</h1>
                <div className="row">
                    {
                        this.state.services.map(service => (
                            <div className="col-md-4 p-2" key={service._id}>
                                <div className="card">
                                    <div className="card-header d-flex justify-content-between">
                                        <h5>{service.state}</h5>
                                        <Link to={"/edit/" + service._id} className="btn btn-secondary edit" style={{'display': this.state.useCase ? 'block' : 'none'}}>
                                            <i className="material-icons">
                                                border_color</i>
                                        </Link>
                                    </div>
                                    <div className="card-body">
                                        <p>
                                            {service.indications}
                                        </p>
                                        <p>
                                            {service.description}
                                        </p>
                                        <p>
                                            {service.comment}
                                        </p>
                                        <p>
                                            {service.calification}
                                        </p>
                                        <p>
                                            {format(service.createdAt)}
                                        </p>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-danger" onClick={() => this.deleteService(service._id)} style={{'display': this.state.useCase ? 'block' : 'none'}}>
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </React.Fragment>
        )
    }
}
