import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import swal from 'sweetalert';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import axios from 'axios'

export default class CreateService extends Component {

    state = {
        indication: '',
        description: '',
        date: new Date(),
        employee: '',
        employees: [],
        editing: false,
        _idUserLogged: '',
        comment: '',
        calification: '',
        idService: '',
        title: '',
        button: ''
    }

    redirect = false;

    async componentDidMount() {
        if ( !this.props.match.params.id && this.props.location.state._idUserLogged ) {
            // const clients = await axios.get(`http://localhost:4000/api/users/getUser/${this.props.location.state._id}`);
            this.setState({
                _idUserLogged: this.props.location.state._idUserLogged
            });
        }

        // const employees = await axios.get('http://localhost:4000/api/users/getEmployees');
        // console.log(employees)
        // if (employees.data.ok) {
        //     this.setState({
        //         employees: employees.data.users.map(user => user.name),
        //     })
        // }
        if (this.props.match.params.id) {
            const res = await axios.get(`http://localhost:4000/api/services/getService/${this.props.match.params.id}`);
            this.setState({
                _idUserLogged: res.data.service.idClient
            });
            console.log(res)
            this.setState({
                indication: res.data.service.indications,
                description: res.data.service.description,
                date: res.data.service.createdAd,
                comment: res.data.service.comment,
                calification: res.data.service.calification,
                editing: true,
                idService: this.props.match.params.id,
            });
            swal("Hi!", "You can make comment and rate the service you chosed moments ago!", "info");
        }

        if ( this.state.editing ) {
            this.state.title = 'Edit Service';
            document.querySelector(".indication").style.visibility = 'hidden';
            document.querySelector(".indication").style.display = 'none';
            document.querySelector(".description").style.visibility = 'hidden';
            document.querySelector(".description").style.display = 'none';
        } else {
            this.state.title = 'Create Service'
            document.querySelector(".comment").style.visibility = 'hidden';
            document.querySelector(".comment").style.display = 'none';
            document.querySelector(".calification").style.visibility = 'hidden';
            document.querySelector(".calification").style.display = 'none';
        }
    }

    onSubmit = async (e) => {
        e.preventDefault();
        if (this.state.editing) {
            console.log(this.state)
            const service = {
                indication: this.state.indication,
                description: this.state.description,
                comment: this.state.comment,
                calification: this.state.calification,
                idService: this.state.idService,
                date: this.state.date
            };
            const updatedService = await axios.put('http://localhost:4000/api/services/updateService', service);
            if ( updatedService.data.ok ) {
                this.redirect = true;
                this.setState({
                    employee: ''
                });
            }
        } else {
            const newService = {
                indications: this.state.indication,
                description: this.state.description,
                idClient: this.state._idUserLogged,
                date: this.state.date
            };
            const createdService = await axios.post('http://localhost:4000/api/services/createService', newService);
            if ( createdService.data.ok ) {
                this.redirect = true;
                this.setState({
                    employee: ''
                });
            }
        }
        // window.location.href = '/servicesList';

    }

    onInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeDate = date => {
        this.setState({ date });
    }

    render() {
        if ( this.redirect ) {
            return <Redirect to={{
                pathname: '/servicesList',
                state: { 
                    _idUserLogged: this.state._idUserLogged,
                }
            }}/>;
        }
        return (
            <div className="col-md-6 offset-md-3">
                <div className="card card-body">
                    <h4>{this.state.title}</h4>
                    <form onSubmit={this.onSubmit}>
                        {/* SELECT THE EMPLOYEE */}
                        {/* <div className="form-group">
                            <select
                                className="form-control"
                                value={this.state.employee}
                                onChange={this.onInputChange}
                                name="employee"
                                required>
                                {
                                    this.state.employees.map(user => (
                                        <option key={user} value={user}>
                                            {user}
                                        </option>
                                    ))
                                }
                            </select>
                        </div> */}
                        {/* Service Indications */}
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control indication"
                                placeholder="Indications, where and when do you need the service."
                                onChange={this.onInputChange}
                                name="indication"
                                value={this.state.indication}/>
                        </div>
                        {/* Service Description */}
                        <div className="form-group">
                            <textarea
                                type="text"
                                className="form-control description"
                                placeholder="Description of the service do you need."
                                name="description"
                                onChange={this.onInputChange}
                                value={this.state.description}>
                            </textarea>
                        </div>
                        {/* Service Comment */}
                        <div className="form-group">
                            <textarea
                                type="text"
                                className="form-control comment"
                                placeholder="Comments of the service you received."
                                name="comment"
                                onChange={this.onInputChange}
                                value={this.state.comment}>
                            </textarea>
                        </div>
                        {/* Service Calification */}
                        <div className="form-group">
                            <input
                                type="number"
                                className="form-control calification"
                                placeholder="Calificate service."
                                name="calification"
                                onChange={this.onInputChange}
                                value={this.state.calification}/>
                        </div>
                        {/*  Date */}
                        <div className="form-group">
                            <DatePicker className="form-control" selected={this.state.date} onChange={this.onChangeDate} />
                        </div>
                        <button className="btn btn-primary">
                            {this.state.title} <i className="material-icons">assignment</i>
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}
