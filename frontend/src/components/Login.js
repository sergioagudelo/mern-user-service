import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import swal from 'sweetalert';
import axios from 'axios'

export default class Login extends Component {

    state = {
        name: '',
        email: '',
        cellNumber: '',
        password: '',
        rol: '',
        users: [],
        _id: ''
    }

    redirect = false;

    async componentDidMount() {
        this.getUsers();
    }

    getUsers = async () => {
        const res = await axios.get('http://localhost:4000/api/users/getUsers');
        this.setState({
            users: res.data.users
        });
        
    }
    
    onInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    doubleClick = (user) => {
        // console.log("aaa",document.querySelector('.name'))
       this.setState({
            name: user.name,
            email: user.email,
            cellNumber: user.cellNumber,
            password: '123456',
            rol: user.rol,
       });
    }

    setDefaultState = () => {
        this.setState({
            name: '',
            email: '',
            cellNumber: '',
            password: '',
            rol: '',
        });
    }

    onSubmit = async (e) => {
        e.preventDefault();
        await axios.post('http://localhost:4000/api/users/loginUser', {
            email: this.state.email,
            password: this.state.password
        }).then(user =>{
            if ( user.data.ok ) {
                this.setState({
                    _id: user.data.user._id
                });
                localStorage.setItem('rol',  user.data.user.rol);
                this.redirect = true;
            } else {
                swal("Oops!", "Something went wrong!", "error");
            }
        });
        this.setDefaultState();
    }

    render() {
        if ( this.redirect ) {
            if ( localStorage.getItem('rol') === 'CLIENT_ROLE' ) {
                return <Redirect to={{
                    pathname: '/createService',
                    state: { _idUserLogged: this.state._id}
                }}/>;
            } else {
                return <Redirect to={{
                    pathname: '/servicesList/',
                    state: { _idUserLogged: this.state._id}
                }}/>;
            }
        }
        return (
            <div className="row">
                <div className="col-md-5">
                    <div className="card card-body">
                        <h3>Login</h3>
                        <p>Only <b>email</b> and <b>password</b> are neccesary. You can find the users on the right side of the window. Try double clicking the user yo want to logging with.</p>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <input
                                    className="form-control name"
                                    name="name"
                                    value={this.state.name}
                                    type="text"
                                    onChange={this.onInputChange}
                                    placeholder="name"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    className="form-control email"
                                    name="email"
                                    value={this.state.email}
                                    type="text"
                                    onChange={this.onInputChange}
                                    placeholder="email"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    className="form-control cellNumber"
                                    name="cellNumber"
                                    value={this.state.cellNumber}
                                    type="text"
                                    onChange={this.onInputChange}
                                    placeholder="cellNumber"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    className="form-control password"
                                    name="password"
                                    value={this.state.password}
                                    type="text"
                                    onChange={this.onInputChange}
                                    placeholder="password"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    className="form-control rol"
                                    name="rol"
                                    value={this.state.rol}
                                    type="text"
                                    onChange={this.onInputChange}
                                    placeholder="rol"
                                />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Login
                    </button>
                        </form>
                    </div>
                </div>
                <div className="col-md-7">
                    <ul className="list-group">
                        {
                            this.state.users.map(user => (
                                <li className="list-group-item list-group-item-action" key={user._id} onDoubleClick={() => this.doubleClick(user)}>
                                    <b>name: </b>{user.name} - <b>email: </b>{user.email} - <b>cellNumber: </b>{user.cellNumber} - <b>password: </b> 123456 - <b style={{color: 'red'}}>rol: </b> {user.rol}
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
        )
    }
}
