import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Navigation from './components/Navigation'
import ServicesList from './components/servicesList'
import CreateService from './components/CreateService'

import './App.css';
import Login from './components/Login';

function App() {
  return (
    <Router>
      <Navigation />
      <div className="container py-4">
        <Route path="/" exact component={Login} />
        <Route path="/createService" component={CreateService} />
        <Route path="/edit/:id" component={CreateService} />
        <Route path="/servicesList" component={ServicesList} />
      </div>
    </Router>
  );
}

export default App;
